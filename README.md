# README #


All_DATA : Raw data without Cleaning

Cleaned_DATA_for_LLT : Cleaned data with features used by the LLT Model (e.g. distance, theta)

Cleaned_DATA_for_MPC : Cleaned data with features used by the MPC Model (e.g. previous_cell, next_cell)

The dataset is described in the paper : "Effective Prediction of V2I Link Lifetime and Vehicle’s Next Cell for Software Defined Vehicular Networks: A Machine Learning Approach" : https://hal.laas.fr/hal-02360810/document

For more information, please contact : soufian.toufga@laas.fr

